import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "mobx-react";
import {Router, Route, Switch, Redirect} from "react-router-dom";
import {getSnapshot} from "mobx-state-tree";
import {inject} from "mobx-react";

import {SheetContainer} from "app/components/sheet-container";
import {RootStore, Workspace} from "app/models";

import {createBrowserHistory} from 'history';
import {RouterStore, syncHistoryWithStore} from 'mobx-react-router';

// import "preact/devtools";

import "assets/index.scss";
import { InjectedProps } from "app/components/injected-props";

const browserHistory = createBrowserHistory();
const routingStore = new RouterStore();

let store = RootStore.create({
  // TODO: This is optional, how to initialize properly?
  workspace: {sheetSet: {}, sheetStack: []},
  componentRegistry: {registry: {}, showComponentRegions: true}
});
// @ts-ignore
window.store = store;

const history = syncHistoryWithStore(browserHistory, routingStore);

require('./modules/Sidebar.tsx');

@inject("store")
class LayoutRoute extends React.Component<any> {
  get injected() {
    return (this.props as any) as InjectedProps;
  }

  componentWillMount() {
    this.updateSheet();
  }

  componentDidUpdate() {
    this.updateSheet();
  }

  updateSheet() {
    const {workspace} = this.injected.store;
    const sheet = workspace.sheetSet.get(this.props.layout);
    if (sheet) {
      workspace.bringToTop(sheet);
    }
  }

  render() {
    return (<Route {...this.props} />);
  }
}

initializeBasicSheet(store.workspace);
renderApp();

function renderApp() {
  ReactDOM.render(
    <Provider store={store} routing={routingStore}>
      <Router history={history}>
        <Switch>
          <LayoutRoute path='/layout1' layout='Layout1' component={SheetContainer} />  
          <LayoutRoute path='/layout2' layout='Layout2' component={SheetContainer} />
          <LayoutRoute path='/layout3' layout='Layout3' component={SheetContainer} />
          <Redirect from="/" exact to="/layout1" />
        </Switch>
      </Router>
    </Provider>,
    document.getElementById('root')
  );
}

function initializeBasicSheet(workspace: typeof Workspace.Type) {
}

if (module.hot) {
  module.hot.accept();
  module.hot.accept(["./app/models"], (...args) => {
    const snapsot = getSnapshot(store);
    store = RootStore.create(snapsot);
    // @ts-ignore
    window.store = store;
    renderApp();
  });
}
