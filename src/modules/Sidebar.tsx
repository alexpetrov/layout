import * as React from "react";
import {toJS} from 'mobx';
import {observer} from "mobx-react";
import {inject} from "mobx-react";
import {InjectedProps} from '../app/components/injected-props';
import {Link} from 'react-router-dom';

import {NavigationBar} from "app/components/navigation-bar";

export class ResultList extends React.Component {
  static displayName = 'ResultList';

  static containerStyles = {
    minWidth: 354,
    maxWidth: 354,
  };

  render() {
    return <h2 style={{color: "white"}}>ResultList</h2>;
  }
}

export class ResultListToolbar extends React.Component {
  static displayName = 'ResultListToolbar';

  render() {
    return <span>ResultList.Toolbar</span>;
  }
}

export class ViewToolbarHeader extends React.Component {
  static displayName = 'ViewToolbarHeader';

  render() {
    return <span>View.ToolbarHeader</span>;
  }
}

export class StatementPanel extends React.Component {
  static displayName = 'StatementList';

  static containerStyles = {
    minWidth: 286,
    maxWidth: 286,
  };

  render() {
    return <h2 style={{color: 'white'}}>StatementList</h2>;
  }
}

export class NoteSplit extends React.Component<any> {
  static displayName = 'NoteSplit';

  static containerStyles = {
    minWidth: 320,
    maxWidth: 320,
  };

  render() {
    return <h2 style={{color: 'white'}}>NoteSplit {this.props.foo}</h2>;
  }
}

export class DocViewer extends React.Component<any> {
  static displayName = 'DocViewer';

  render() {
    return <h2 style={{color: 'white'}}>DocViewer {this.props.passedProp}</h2>;
  }
}

@inject("store")
@observer
export class Toolbar extends React.Component {
  static displayName = 'Toolbar';
  
  get injected() {
    return (this.props as any) as InjectedProps;
  }

  toggleStacking() {
    this.injected.store.workspace.toggleStacking();
  }

  render() {
    const {workspace} = this.injected.store;

    console.log('dostack', workspace.doStackLayouts)

    return (<div className="top-toolbar">
      <input type="checkbox" id="checkbox-usestacking" onChange={() => this.toggleStacking()} checked={workspace.doStackLayouts}></input>
      <label htmlFor="checkbox-usestacking">Use stacking</label>

      <Link to='layout1'>Layout1</Link>
      <Link to='layout2'>Layout2</Link>
      <Link to='layout3'>Layout3</Link>
      
    </div>);
  }
}

export function activate() {
  const w = window as any;

  w.store.componentRegistry.register(Toolbar, {
    location: toJS(w.store.workspace.sheetSet.get('Global').Header),
  });


  w.store.componentRegistry.register(NavigationBar, {
    location: toJS(w.store.workspace.Locations.get('NavigationBar')),
  });

  w.store.componentRegistry.register(ResultList, {
    location: toJS(w.store.workspace.Locations.get('ResultList')),
  });

  w.store.componentRegistry.register(StatementPanel, {
    location: toJS(w.store.workspace.Locations.get('StatementList')),
  });

  w.store.componentRegistry.register(DocViewer, {
    location: toJS(w.store.workspace.Locations.get('DocViewer')),
  });

  w.store.componentRegistry.register(NoteSplit, {
    location: toJS(w.store.workspace.Locations.get('NoteSplit')),
  });
}

activate();
