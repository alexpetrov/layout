import { types } from 'mobx-state-tree';
import { LocId } from './common';

const SheetToolbarDeclaration = types.model({
  Left: LocId,
  Right: LocId,
});

export const SheetColumnLocation = types.model({
  id: types.string,
  Toolbar: LocId,
});

export const Sheet = types.model({
  id: types.identifier,
  columns: types.optional(types.map(types.array(SheetColumnLocation)), {}),
  supportedModes: types.array(types.string),
  icon: types.optional(types.string, ''),
  name: types.optional(types.string, ''),
  root: types.optional(types.boolean, false),

  Toolbar: SheetToolbarDeclaration,
  Header: LocId,
  Footer: LocId,
  Center: types.maybe(LocId),
});

type LayoutMode = 'split' | 'list';

interface Columns {
  [mode: string]: string[];
}

function rootSheet(self) {
  if (self.sheetStack.length) {
    return self.sheetStack[0];
  }
  return undefined;
}

export const Workspace = types
  .model('Workspace', {
    Locations: types.optional(types.map(SheetColumnLocation), {}),
    sheetSet: types.optional(types.map(Sheet), {}),
    sheetStack: types.optional(types.array(types.reference(Sheet)), []),
    stackLayouts: types.optional(types.boolean, true)
  })
  .actions(self => {
    function defineSheet(id: string, options: Partial<typeof Sheet.Type> = {}, columns: Columns = {}) {
      const mappedColumns: Record<string, Array<typeof SheetColumnLocation.Type>> = {};
      for (let layout in columns) {
        mappedColumns[layout] = [];
        const cols = columns[layout];
        for (let idx = 0; idx < cols.length; idx++) {
          const col = cols[idx];
          if (!self.Locations.has(col)) {
            self.Locations.set(col, {
              id: `${col}`,
              Toolbar: { id: `${col}:Toolbar` },
            });
          }
          mappedColumns[layout][idx] = {
            id: `${col}`,
            Toolbar: { id: `${col}:Toolbar` },
          };
        }
      }

      self.sheetSet.set(id, {
        id,
        columns: mappedColumns,
        supportedModes: Object.keys(columns),
        icon: options.icon,
        name: options.name,
        root: options.root,

        Toolbar: {
          Left: { id: `Sheet:${id}:Toolbar:Left` },
          Right: { id: `Sheet:${id}:Toolbar:Right` },
        },
        Header: { id: `Sheet:${id}:Toolbar:Header` },
        Footer: { id: `Sheet:${id}:Toolbar:Footer` },
        Center: undefined,
      });

      if (options.root && !rootSheet(self) && !(options as any).silent) {
        onSelectRootSheet(self.sheetSet.get(id));
      }
    }

    function resetInstanceVars() {
      self.sheetSet.clear();
      self.sheetStack.clear();

      defineSheet('Global');
      defineSheet(
        'Layout1',
        { root: true },
        {
          split: ['NavigationBar', 'ResultList', 'StatementList', 'DocViewer', 'NoteSplit'],
        },
      );

      defineSheet(
        'Layout2',
        {
          root: true
        },
        {
          split: ['NavigationBar', 'Layout2-Column1', 'Layout2-Column2']
        }
      );

      defineSheet(
        'Layout3',
        { 
          root: true
        },
        {
          split: ['NavigationBar', 'Layout3-Column1', 'DocViewer']
        }
      )
    }

    function onSelectRootSheet(sheet: typeof Sheet.Type) {
      if (!sheet) {
        console.error('onSelectRootSheet: sheet is not a valid sheet');
        return;
      }
      if (!sheet.root) {
        console.error('onSelectRootSheet: sheet is not registered as root sheet');
        return;
      }
      self.sheetStack.clear();
      self.sheetStack.push(sheet);
    }

    function bringToTop(sheet: typeof Sheet.Type) {
      if (!self.stackLayouts) {
        self.sheetStack.length = 0;
      }
      if (!self.sheetStack.includes(sheet)) {
        // self.sheetStack.length = 0;
        self.sheetStack.push(sheet.id);
      } else {
        self.sheetStack.splice(self.sheetStack.indexOf(sheet), 1);
        self.sheetStack.push(sheet.id);
      }
    }

    function toggleStacking() {
      self.stackLayouts = !self.stackLayouts;
    }

    return {
      afterCreate() {
        resetInstanceVars();
      },
      defineSheet,
      bringToTop,
      toggleStacking
    };
  })
  .views(self => ({
    get doStackLayouts(): boolean {
      return self.stackLayouts;
    },

    get layoutMode(): LayoutMode {
      return 'split';
    },

    get globalSheet() {
      return self.sheetSet.get('Global');
    },

    isLocationHidden(location: typeof SheetColumnLocation.Type) {
      // TODO: Implement
      return false;
    },
  }));