import { types } from 'mobx-state-tree';
import { values } from 'mobx';
import { LocId } from './common';
// import { isProduction } from 'app/env';

export interface ComponentDescriptor {
  role?: string;
  roles?: string[];
  mode?: string;
  modes?: string[];
  location?: typeof LocId.Type;
  locations?: Array<typeof LocId.Type>;
}

const ComponentRegistryEntry = types.model({
  displayName: types.identifier,
  modes: types.optional(types.array(types.string), []),
  locations: types.optional(types.array(LocId), []),
  roles: types.optional(types.array(types.string), []),
});

const componentMap: Record<string, React.ComponentType> = {};

export const ComponentRegistry = types
  .model('ComponentRegistry', {
    registry: types.map(ComponentRegistryEntry),
    showComponentRegions: types.optional(types.boolean, true),
  })
  .actions(self => {
    return {
      register(component: React.ComponentType, options: ComponentDescriptor) {
        if (!component.displayName) {
          throw new Error('ComponentRegistry.register() requires that your React component defines a `displayName`');
        }
        const { locations, modes, roles } = pluralizeDescriptor(options);
        if (!roles && !locations) {
          throw new Error('ComponentRegistry.register() requires `role` or `location`');
        }
        if (self.registry.has(component.displayName)) {
          throw new Error(`ComponentRegistry.register(): A different component was already registered with the name
				${component.displayName}`);
        }

        self.registry.set(component.displayName, {
          displayName: component.displayName,
          locations,
          modes,
          roles,
        });
        componentMap[component.displayName] = component;
      },
      unregister(component: React.ComponentType) {
        self.registry.delete(component.displayName);
        delete componentMap[component.displayName];
      },
      toggleShowComponentRegions() {
        self.showComponentRegions = !self.showComponentRegions;
      },
    };
  })
  .views(self => ({
    findComponentByName(name: string) {
      return self.registry.has(name) && componentMap[name];
    },
    findComponentsMatching(descriptor: Pick<ComponentDescriptor, 'location' | 'mode' | 'role'>) {
      console.log('findMatchingComponents');

      const { locations, modes, roles } = pluralizeDescriptor(descriptor as any);
      if (!locations && !modes && !roles) {
        throw new Error('ComponentRegistry.findComponentsMatching called with empty descriptor');
      }

      const overlaps = <T>(entry: T[] = [], search: T[] = []) => intersection(entry, search).length > 0;

      const entries: Array<any> = values(self.registry).filter((entry: any) => {
        if (modes && entry.modes.length && !overlaps(modes, entry.modes)) {
          return false;
        }
        if (locations && !overlaps(locations.map(x => x.id), entry.locations.map(x => x.id))) {
          return false;
        }
        if (roles && !overlaps(roles, entry.roles)) {
          return false;
        }
        return true;
      });
      return entries.map(x => componentMap[x.displayName]).filter(x => x);
    },
    doShowComponentRegions() {
      // return false;
      return self.showComponentRegions;
    },
  }));

export const ComponentRegistryInst = ComponentRegistry.create();

function pluralizeDescriptor(descriptor: ComponentDescriptor) {
  let { locations, modes, roles } = descriptor;
  if (descriptor.mode) {
    modes = [descriptor.mode];
  }
  if (descriptor.role) {
    roles = [descriptor.role];
  }
  if (descriptor.location) {
    locations = [descriptor.location];
  }
  return { locations, modes, roles };
}

const intersection = <T>(arrA: T[], arrB: T[]) => arrA.filter(x => arrB.includes(x));