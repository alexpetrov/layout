import { types } from "mobx-state-tree";
import { Workspace } from "./Workspace";
import { ComponentRegistry } from "./ComponentRegistry";

export const RootStore = types.model({
  workspace: Workspace,
  componentRegistry: ComponentRegistry
});
