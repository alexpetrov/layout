import { types } from 'mobx-state-tree';

export const LocId = types.model({
  id: types.string,
});