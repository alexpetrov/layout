export function fastOmit<T extends {}>(props: T, without: Array<keyof T>) {
  Object.keys;
  const otherProps = Object.assign({}, props);
  for (let w of without) {
    delete otherProps[w];
  }
  return otherProps;
}
