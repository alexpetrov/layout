import * as React from "react";
import { observer } from "mobx-react";
import { inject } from "mobx-react";
import { InjectedProps } from "./injected-props";

import { Sheet } from "app/models";
import { Flexbox } from "./flexbox";
import { InjectedComponentSet } from "./injected-component-set";

const FLEX = 10000;

interface SheetLocation {
  id: string;
}

interface SheetProps {
  data: typeof Sheet.Type;
  depth: number;
}

interface SheetColumn {
  maxWidth: number;
  minWidth: number;
  location: SheetLocation;
  width: number;
}

interface SheetState {
  mode: string;
  columns: SheetColumn[];
}

@inject("store")
@observer
export class SheetComponent extends React.Component<SheetProps, SheetState> {
  static displayName = "Sheet";

  constructor(props) {
    super(props);
    this.state = this.buildState();
  }

  componentWillReceiveProps(nextProps) {
    this.setState(this.buildState(nextProps));
  }

  get injected() {
    return (this.props as any) as InjectedProps;
  }

  private buildState(props: InjectedProps & SheetProps = this.props as any) {
    const state: SheetState = {
      mode: props.store.workspace.layoutMode,
      columns: []
    };
	
	let widest = -1;
	let widestWidth = -1;

	const data = props.data;
	
    if (data && data.columns.has(state.mode)) {
      data.columns.get(state.mode).forEach((loc, idx) => {
        if (props.store.workspace.isLocationHidden(loc)) {
          return;
        }

        const entries = props.store.componentRegistry.findComponentsMatching({
          location: loc,
          mode: state.mode
        });

		const maxWidth = entries.reduce((m, component) => {
			const containerStyles = component['containerStyles'];
			if (containerStyles && containerStyles.maxWidth !== undefined && containerStyles.maxWidth < m) {
				return containerStyles.maxWidth;
			}
			return m;
		}, 10000);

		const minWidth = entries.reduce((m, component) => {
			const containerStyles = component['containerStyles'];
			if (containerStyles && containerStyles.minWidth !== undefined && containerStyles.minWidth > m) {
				return containerStyles.minWidth;
			}
			return m;
		}, 0);

		// TODO: Get width for each column
        const WIDTH = 350;
        const col: SheetColumn = { maxWidth, minWidth, location: loc, width: WIDTH };
		state.columns.push(col);
		
		if (maxWidth > widestWidth) {
			widestWidth = maxWidth;
			widest = idx;
		}
      });
    }
    if (state.columns.length > 0) {
      state.columns[widest].maxWidth = FLEX;
    }
    return state;
  }

  private columnFlexboxElements() {
    return this.state.columns.map((column, idx) => {
      // @ts-ignore
      const { maxWidth, minWidth, location, width } = column;
      const style: React.CSSProperties = {
        height: "100%",
        minWidth: minWidth,
        overflow: "hidden"
      };
      if (maxWidth < FLEX) {
        style.width = maxWidth;
      } else {
        style.flex = 1;
      }
      return (
        <InjectedComponentSet
          direction="column"
          key={idx}
          name={`${this.props.data.id}:${idx}`}
          className={`column-${location.id}`}
          data-column={idx}
          style={style}
          matching={{ location, mode: this.state.mode }}
          exposedProps={{passedProp: 'foo'}}
        />
      );
    });
  }

  render() {
    const style: React.CSSProperties = {
      position: "absolute",
      width: "100%",
      height: "100%",
      zIndex: 1
    };
    return (
      <div data-role="Sheet" style={style} className={`sheet mode-${this.state.mode}`} data-id={this.props.data.id}>
        <Flexbox direction="row" style={{ overflow: "hidden" }}>
          {this.columnFlexboxElements()}
        </Flexbox>
      </div>
    );
  }
}
