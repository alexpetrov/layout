import { RootStore } from "app/models";

export interface InjectedProps {
  store: typeof RootStore.Type;
}
