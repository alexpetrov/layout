import * as React from "react";
import { inject, observer } from "mobx-react";
import { InjectedProps } from "./injected-props";
import { ComponentDescriptor } from "app/models/ComponentRegistry";
import { Flexbox } from "./flexbox";
import { fastOmit } from "app/object-utils";
import { InjectedComponentErrorBoundary } from "./injected-component-error-boundary";
import { InjectedComponentLabel } from "./injected-component-label";

interface InjectedComponentSetProps {
  matching: ComponentDescriptor;
  id?: string;
  className?: string;
  name?: string;
  matchLimit?: number;
  exposedProps?: object;
  containersRequired?: boolean;
  inline?: boolean;
  direction?: string;
  style?: React.CSSProperties;
}

@inject("store")
@observer
export class InjectedComponentSet extends React.Component<InjectedComponentSetProps> {
  static displayName = "InjectedComponentSet";
  static defaultProps = {
    direction: "row",
    className: "",
    exposedProps: {},
    containersRequired: true
  };

  get injected() {
    return (this.props as any) as InjectedProps;
  }

  render() {
    const registry = this.injected.store.componentRegistry;
    const components = registry.findComponentsMatching(this.props.matching).slice(0, this.props.matchLimit);
    const visible = registry.doShowComponentRegions();

    const flexboxProps = fastOmit(this.props, [
      "matching",
      "children",
      "className",
      "matchLimit",
      "exposedProps",
      "containersRequired"
    ]);

    const { exposedProps, containersRequired, matching, children } = this.props;
    let { className } = this.props;

    const elements = components.map(Component => {
      if (!containersRequired) {
        return <Component key={Component.displayName} {...exposedProps} />;
      }
      return (
        <InjectedComponentErrorBoundary key={Component.displayName}>
          <Component {...exposedProps} />
        </InjectedComponentErrorBoundary>
      );
    });

    if (visible) {
      className += " injected-region-visible";
      elements.unshift(<InjectedComponentLabel key="_label" matching={matching} {...exposedProps} />);
      elements.push(<span key="_clear" style={{ clear: "both" }} />);
    }

    return (
      <Flexbox className={className} {...flexboxProps}>
        {elements}
        {children}
      </Flexbox>
    );
  }
}
