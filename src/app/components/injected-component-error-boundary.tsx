import * as React from "react";

interface InjectedComponentErrorBoundaryState {
  error: string | null;
}

export class InjectedComponentErrorBoundary extends React.Component<{}, InjectedComponentErrorBoundaryState> {
  static displayName = "InjectedComponentErrorBoundary";

  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, info) {
    this.setState({ error: error.stack });
    // TODO: Report error
  }

  render() {
    if (this.state.error) {
      return (
        <div className="unsafe-component-exception">
          <div className="trace">{this.state.error}</div>
        </div>
      );
    }
    return this.props.children;
  }
}
