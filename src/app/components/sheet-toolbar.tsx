import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { observer, inject } from 'mobx-react';

import { Sheet } from 'app/models';
import { Flexbox } from './flexbox';

interface ToolbarProps {
  data: typeof Sheet.Type;
  depth: number;
}

@inject('store')
@observer
export class Toolbar extends React.Component<ToolbarProps> {
  static displayName = 'Toolbar';

  get injected() {
    return (this.props as any);
  }

  private mounted = false;
  private lastReportedToolbarHeight = 0;

  getChildContext() {
    return {
      sheetDepth: this.props.depth,
    };
  }

  componentDidMount() {
    this.mounted = true;
    window.addEventListener('resize', this.onWindowResize);
    window.requestAnimationFrame(() => this.recomputeLayout());
  }

  componentDidUpdate() {
    window.requestAnimationFrame(() => this.recomputeLayout());
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  recomputeLayout = () => {
    if (!this.mounted) {
      return;
    }
    const el = ReactDOM.findDOMNode(this) as HTMLElement;
    const columnToolbarEls = el.querySelectorAll('[data-column]');

    const sheet = document.querySelectorAll("[data-role='Sheet']")[this.props.depth];
    if (!sheet) {
      return;
    }
    for (const columnToolbarEl of Array.from(columnToolbarEls) as HTMLElement[]) {
      const column = columnToolbarEl.dataset.column;
      const columnEl = sheet.querySelector(`[data-column='${column}']`) as HTMLElement;
      if (!columnEl) {
        continue;
      }
      columnToolbarEl.style.display = 'inherit';
      columnToolbarEl.style.left = `${columnEl.offsetLeft}px`;
      columnToolbarEl.style.width = `${columnEl.offsetWidth}px`;
    }
    if (el.clientHeight !== this.lastReportedToolbarHeight) {
      this.lastReportedToolbarHeight = el.clientHeight;
    }
  };

  render() {
    const state = this.buildStateFromProps();
    const toolbars = state.columns.map((components, idx) => (
      <div
        style={{ position: 'absolute', top: 0, display: 'none' }}
        className={`toolbar-${state.columnNames[idx]}`}
        data-column={idx}
        key={idx}
      >
        {this.flexBoxForComponents(components)}
      </div>
    ));
    return (
      <div
        style={{ position: 'absolute', width: '100%', height: '100%', zIndex: 1 }}
        className={`sheet-toolbar-container mode-${state.mode}`}
        data-id={this.props.data.id}
      >
        {toolbars}
      </div>
    );
  }

  private onWindowResize = () => {
    this.recomputeLayout();
  };

  private buildStateFromProps(props: any & ToolbarProps = this.props as any) {
    const state = {
      mode: props.store.workspace.layoutMode,
      columns: [],
      columnNames: [],
    };
    if (props.data && props.data.columns.has(state.mode)) {
      for (const loc of props.data.columns.get(state.mode)) {
        if (props.store.workspace.isLocationHidden(loc)) {
          continue;
        }
        const entries = props.store.componentRegistry.findComponentsMatching({
          location: loc.Toolbar,
          mode: state.mode,
        });
        state.columns.push(entries);
        if (entries) {
          state.columnNames.push(loc.Toolbar.id.split(':')[0]);
        }
      }
    }

    if (state.columns.length > 0) {
      for (const loc of [props.store.workspace.globalSheet, props.data]) {
        const entries = props.store.componentRegistry.findComponentsMatching({
          location: loc.Toolbar.Left,
          mode: state.mode,
        });
        state.columns[0].push(...entries);
      }
      for (const loc of [props.store.workspace.globalSheet, props.data]) {
        const entries = props.store.componentRegistry.findComponentsMatching({
          location: loc.Toolbar.Right,
          mode: state.mode,
        });
        state.columns[state.columns.length - 1].push(...entries);
      }
    }
    return state;
  }

  private flexBoxForComponents = (components: any[]) => {
    const elements = components.map(Component => <Component key={Component.displayName} {...this.props} />);
    return (
      <Flexbox className="item-container" direction="row">
        {elements}
        <ToolbarSpacer key="spacer-50" order={-50} />
        <ToolbarSpacer key="spacer+50" order={50} />
      </Flexbox>
    );
  };
}

class ToolbarSpacer extends React.Component<{ order: number }> {
  static displayName = 'ToolbarSpacer';
  render() {
    return <div className="item-spacer" style={{ flex: 1, order: this.props.order || 0 }}></div>;
  }
}