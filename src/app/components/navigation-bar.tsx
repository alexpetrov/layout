import * as React from 'react';
import {observer} from "mobx-react";
import {inject} from "mobx-react";
import {InjectedProps} from './injected-props';

@inject("store")
@observer
export class NavigationBar extends React.Component {
  static displayName = "NavigationBar";
  static containerStyles = {
    minWidth: 60,
    maxWidth: 60,
  }

  get injected() {
    return (this.props as any) as InjectedProps;
  }

  changeLayout = (layout: string) => {
    const {workspace} = this.injected.store;
    const sheet = workspace.sheetSet.get(layout);
    if (sheet) {
      workspace.bringToTop(sheet);
    }
  }

  render() {
    return (
      <div className="navigation-bar flex-filler">
        <button style={{height: '40px', width: '40px'}} onClick={() => this.changeLayout('Layout1')}>1</button>
        <button style={{height: '40px', width: '40px'}} onClick={() => this.changeLayout('Layout2')}>2</button>
        <button style={{height: '40px', width: '40px'}} onClick={() => this.changeLayout('Layout3')}>3</button>
      </div>
    );
  }
};