import * as React from "react";

import { fastOmit } from "app/object-utils";

type FlexboxProps = {
  direction?: string;
  inline?: boolean;
  style?: object;
  height?: string;
};

export class Flexbox extends React.Component<FlexboxProps & React.HTMLProps<HTMLDivElement>> {
  static displayName = "Flexbox";
  static defaultProps = {
    height: "100%",
    style: {}
  };

  render() {
    const style = Object.assign(
      {},
      {
        flexDirection: this.props.direction,
        position: "relative",
        display: "flex",
        height: this.props.height
      },
      this.props.style
    );

    if (this.props.inline === true) {
      style.display = "inline-flex";
    }

    const otherProps = fastOmit(this.props, ["direction", "inline", "style", "height"]);

    return (
      <div style={style} {...otherProps}>
        {this.props.children}
      </div>
    );
  }
}
