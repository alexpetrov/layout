import * as React from 'react';
import { inject, observer } from 'mobx-react';

import { SheetComponent } from './sheet-component';
import { Toolbar } from './sheet-toolbar';
import { Flexbox } from './flexbox';
import { InjectedComponentSet } from './injected-component-set';

interface SheetContainerProps {}

@inject('store')
@observer
export class SheetContainer extends React.Component<SheetContainerProps> {
  static displayName = 'SheetContainer';

  get injected() {
    return this.props as any;
  }

  componentWillUnmount() {
    console.log('SheetContainer will unmount');
  }

  render() {
    const workspace = this.injected.store.workspace;
    const totalSheets = workspace.sheetStack.length;
    const topSheet = workspace.sheetStack[totalSheets - 1];
    if (!topSheet) {
      return <div />;
    }

    const sheetComponents = workspace.sheetStack.map((sheet, index) => (
      <SheetComponent data={sheet} depth={index} key={index > 0 ? `${index}:${sheet.id}` : `root`} />
    ));

    return (
      <Flexbox direction="column" className={`layout-mode-${workspace.layoutMode}`} style={{ overflow: 'hidden' }}>
        {this.toolbarContainerElement()}

        <div style={{ order: 1, zIndex: 2 }}>
          <InjectedComponentSet
            matching={{
              locations: [topSheet.Header, workspace.globalSheet.Header],
            }}
            direction="column"
            id={topSheet.id}
          />
        </div>

        <div style={{ order: 2, flex: 1, position: 'relative', zIndex: 1 }}>{sheetComponents}</div>

        <div style={{ order: 3, zIndex: 4 }}>
          <InjectedComponentSet
            matching={{
              locations: [topSheet.Footer, workspace.globalSheet.Footer],
            }}
            direction="column"
            id={topSheet.id}
          />
        </div>
      </Flexbox>
    );
  }

  private toolbarContainerElement = () => {
    const workspace = this.injected.store.workspace;
    const components = workspace.sheetStack.map((sheet, index) => (
      <Toolbar data={sheet} key={`${index}:${sheet.id}:toolbar`} depth={index} />
    ));
    return (
      <div className="sheet-toolbar" style={{ order: 0, zIndex: 3 }}>
        {components}
      </div>
    );
  };
}